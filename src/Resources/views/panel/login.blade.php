@extends('MPCorePanel::inc.app')
@section('content')
<div class="row h-100">
    <div class="col-sm-12 my-auto">
        <div class="w-100">
                <div style="margin:0 auto;width:500px !important;">
                    @include("MPCorePanel::inc.errors")
                </div>
                <div class="login">
                    {!! Form::open() !!}
                    <img  id="center" src="{!! asset("vendor/mediapress/images/logo.png") !!}" alt="logo">
                    @csrf
                    <div class="form-group">
                        {!! Form::text('email',null,['placeholder'=>trans("MPCorePanel::general.email.address")]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password('password',['placeholder'=>trans("MPCorePanel::general.password")]) !!}
                    </div>
                    <div class="form-group">
                        <div class="submit">
                            {!! Form::submit(trans("MPCorePanel::general.login"),['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
        </div>
    </div>
</div>
@endsection

@push("styles")
    <style>
        body {
            background: #f3dc19 !important;
        }
        #center
        {
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
        .login
        {
            background-color: #fff;
            padding:30px;
            width:500px !important;
            min-height: 280px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            margin-left:auto;
            margin-right:auto;
        }
        form .form-group input{
            /*Fix text on bottom*/
            padding: 25px 0 0 0;
            font: 18px Comfortaa-Light;
        }

        .h-100 {
            /*100vh is 100% with seen screen - 157 = header.height + page.margin.bottom*/
            min-height: calc(100vh - 157px) !important;
        }

        /*Custom css take in bootstrap 4.0*/
        .align-self-center {
            -ms-flex-item-align: center !important;
            align-self: center !important;
        }

        .d-flex {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
        }

        /*Same as in mediaclick-dist.css*/
        form .form-group {
            float: left;
            width: 100%;
            position: relative
        }

        form .form-group.focus label {
            -webkit-transition: 0.2s;
            -o-transition: 0.2s;
            transition: 0.2s;
            top: 0;
            color: #2196f3
        }

        form .form-group.focus input {
            -webkit-transition: 0.2s;
            -o-transition: 0.2s;
            transition: 0.2s;
            border-color: #2196f3
        }

        form .form-group label {
            -webkit-transition: 0.2s;
            -o-transition: 0.2s;
            transition: 0.2s;
            font: 15px Comfortaa-Light;
            color: #646464;
            position: absolute;
            top: 18px
        }

        form .form-group input {
            -webkit-transition: 0.2s;
            -o-transition: 0.2s;
            transition: 0.2s;
            outline: 0;
            height: 50px;
            background: none;
            width: 100%;
            border: none;
            border-bottom: 1px solid #b5b5b5;
        }

        .submit {
            float: left;
            width: 100%;
            margin: 15px 0 0 0
        }

        .submit input[type="submit"] {
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
            transition: 0.3s;
            background: #2196f3;
            cursor: pointer;
            border: 1px solid #2196f3;
            font-weight: inherit !important;
            color: #fff;
            font: 17px Comfortaa-Regular;
            border-radius: 6px;
            width: 100%;
            padding: 7px;
            text-align: center;
            min-height: 20px !important;
        }

        .submit input[type="submit"]:hover {
            -webkit-transition: 0.3s;
            -o-transition: 0.3s;
            transition: 0.3s;
            background: #f3dc19;
            border-color: #f3dc19
        }
    </style>
@endpush