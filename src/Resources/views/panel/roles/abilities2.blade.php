@extends("MPCorePanel::inc.app")
@push("styles")
    <link rel="stylesheet" href="{!! asset("/vendor/mediapress/css/selector.css") !!}"/>
    <link rel="stylesheet" href="{!! asset("/vendor/mediapress/css/jquery/jquery-ui.css") !!}">
@endpush
@php
    $forbidden_abilities = $role->getForbiddenAbilities()->pluck("name")->toArray();
    $abilities = $role->getAbilities(false)->pluck("name")->toArray();
    $forbidden = null; $allowed = null;
    $forbidden = in_array("*", $forbidden_abilities) ? true : false;

    if( ! $forbidden){
        $allowed = in_array("*", $abilities) || $role->can("*") ? true : false;
    }
@endphp

@push('scripts')
    <script src="{!! asset("/vendor/mediapress/js/jquery-ui.js") !!}"></script>
    <script>
        $(function () {
            $('[name=mode]').on('ifChecked', function () {
                var mode = $(this).val();
                switch (mode) {
                    case "everything":
                    case "nothing":
                        $('div#accordion').slideUp(200);
                        break;
                    case "custom":
                        $('div#accordion').slideDown(200);
                        break;
                    default:
                        break;
                }
            });
            $('[name=mode]:checked').trigger('ifChecked');
        });
    </script>
@endpush

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{{ $title }} - {{ $role->title }} ({{ $role->name }})</div>
        <a href="{{route("Keeper.roles.index")}}" class="btn btn-primary"><i
                class="fa fa-arrow-left"></i> {{trans("MPCorePanel::general.back")}}</a>
        <form action="{{route("Keeper.roles.store_abilities", $role->id)}}" class="form-horizontal" id="saveAbilities" autocomplete="off" method="post">
        {!! csrf_field() !!}

        <div class="form-group">
            <div class="checkbox">
                <label class="active">
                    <input type="radio" name="mode"
                           value="everything" {!! $allowed ? "checked" : "" !!}>
                    {{trans('KeeperPanel::roles.ability.allow')}}
                </label>
                <label class="passive">
                    <input type="radio" name="mode" value="nothing" {!! $forbidden ? "checked" : "" !!}>
                    {{trans('KeeperPanel::roles.ability.no_permission')}}
                </label>
                <label>
                    <input type="radio" name="mode"
                           value="custom" {!! (!($allowed || $forbidden) ? "checked" : "") !!}>
                    {{trans('KeeperPanel::roles.ability.special')}}
                </label>
            </div>
        </div>
        <div id="accordion">
            <style>
                .card-body{
                    min-height: 150px;
                }
                #loader-overlay {
                    display: block;
                    position: absolute;
                    background-color:#fff;
                    width: 100%;
                    height: auto;
                    min-height: 150px;
                    top:36px;
                    left:0;
                    bottom:0;
                    font: 15px Comfortaa-Regular;

                }

                #loader-overlay div#loader-center {
                    width: 150px;
                    min-height: 110px;
                    position: absolute;
                    margin: auto;
                    top: 20px;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    text-align: center;
                }

                #loader-overlay div#loader-center img {
                    width: 60px;
                    height: 60px;
                    border-radius: 30px;

                    -webkit-animation-name: spin;
                    -webkit-animation-duration: 2000ms;
                    -webkit-animation-iteration-count: infinite;
                    -webkit-animation-timing-function: linear;
                    -moz-animation-name: spin;
                    -moz-animation-duration: 2000ms;
                    -moz-animation-iteration-count: infinite;
                    -moz-animation-timing-function: linear;
                    -ms-animation-name: spin;
                    -ms-animation-duration: 2000ms;
                    -ms-animation-iteration-count: infinite;
                    -ms-animation-timing-function: linear;

                    animation-name: spin;
                    animation-duration: 2000ms;
                    animation-iteration-count: infinite;
                    animation-timing-function: linear;
                }

                #loader-overlay div#loader-center p#loader-text {
                    display: block;
                    width: 100%;
                    position: absolute;
                    top: 75px;
                    color: #999;
                }

                @-ms-keyframes spin {
                    0% {
                        -ms-transform: rotate(0deg);
                        border-radius: 10px;
                    }
                    50% {
                        -ms-transform: rotate(180deg);
                        border-radius: 30px;
                    }
                    100% {
                        -ms-transform: rotate(360deg);
                        border-radius: 10px;
                    }
                }

                @-moz-keyframes spin {
                    0% {
                        -moz-transform: rotate(0deg);
                        border-radius: 10px;
                    }
                    50% {
                        -moz-transform: rotate(180deg);
                        border-radius: 30px;
                    }
                    100% {
                        -ms-transform: rotate(360deg);
                        border-radius: 10px;
                    }
                }

                @-webkit-keyframes spin {
                    0% {
                        -webkit-transform: rotate(0deg);
                        border-radius: 10px;
                    }
                    50% {
                        -webkit-transform: rotate(180deg);
                        border-radius: 30px;
                    }
                    100% {
                        -ms-transform: rotate(360deg);
                        border-radius: 10px;
                    }
                }

                @keyframes spin {
                    0% {
                        transform: rotate(0deg);
                        border-radius: 10px;
                    }
                    50% {
                        transform: rotate(180deg);
                        border-radius: 30px;
                    }
                    100% {
                        transform: rotate(360deg);
                        border-radius: 10px;
                    }
                }
            </style>

{{--            <div class="card" id="cardKeeper" data-module="Keeper">
                <div class="card-header" id="headingKeeper">
                    <h5 class="mb-0">
                        <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseKeeper"
                                aria-expanded="true" aria-controls="collapseKeeper">
                            Rol ve Yetkiler
                        </button>
                    </h5>
                </div>
                <div id="collapseKeeper" class="collapse show" aria-labelledby="headingKeeper" data-parent="#accordion"
                     style="">
                    <div class="card-body">
                        <div id="accordion_5d398148ef767"></div>

                    </div>
                </div>
            </div>--}}
            @foreach($actions as $key => $action)
                <div class="card subs-unloaded" id="card{{$key}}" data-module="{{$key}}">
                    <div class="card-header" id="heading{{ $key }}">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link" data-toggle="collapse"
                                    data-target="#collapse{{ $key}}" aria-expanded="false"
                                    aria-controls="collapse{{ Str::title($key)}}">
                                {{ $action["name"] }}
                            </button>
                        </h5>
                    </div>
                    <div id="collapse{{ $key }}" class="collapse" aria-labelledby="heading{{ $key }}"
                         data-parent="#accordion">
                        <div class="card-body">

                        </div>
                    </div>
                </div>
            @endforeach
            {{--@php dump($actions); @endphp--}}
        </div>
        <div class="float-right">
            <button class="btn btn-primary">{{trans('MPCorePanel::general.save')}}</button>
        </div>
        </form>
    </div>
    <div>
        <pre><code>
                @php
                    //dd(\DB::getQueryLog());
                @endphp
            </code></pre>
    </div>
@endsection
@push("scripts")
    <script>
        $(function () {
            $(".accordion").accordion({
                heightStyle: "content"
            });
        });

        var token;

        $(document).ready(function(){
            token = $("meta[name='csrf-token']").attr("content");
        });

        function fillModuleSubs(module_key_) {
            var el = $("#card" + module_key_);
            if (!el.length) {
                console.error(module_key_ + " modülü eylemlerini doldurmak için #card" + module_key_ + " seçicisiyle nesne arandı ancak bulunamadı");
                return false;
            }

            var loader = '<div id="loader-overlay" style="display:none;"><div id="loader-center"><img src="{{ asset("/vendor/mediapress/images/default.jpg") }}"/><p id="loader-text">Yükleniyor...</p></div></div>';
            $('.card-body', el).html(loader);
            var loader_el = $("#loader-overlay",el);
            console.log("Veri çekmeye başla",new Date());
            loader_el.fadeIn(1000,function(){
                $.ajax({
                    url: '{{ route('Keeper.roles.module_actions', $id) }}',
                    method: 'POST',
                    data: {module_key: module_key_, _token: token},
                    success: function (data) {
                        console.log("Çekme başarılı....",new Date());
                        $(".card-body", el).append(data).animate(
                          {'background-color':'#f3dc19'},300,
                          function() {
                              $(this).animate({'background-color': '#fff'}, 500);
                          }
                        );
                        $(".accordion").accordion({
                            heightStyle: "content"
                        });
                        console.log("Akordeonlar tamam.",new Date());
                        $('.show > .card-body > .form-group > .checkbox', el).iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue'
                        });

                        console.log("Checkboxlar tamam.",new Date());
                        loader_el.fadeOut(1000);
                        console.log("Basma işlemi tamam",new Date());
                    },
                    error: function (xhr, status, errorThrown) {
                        console.log(errorThrown);
                        loader_el.fadeOut(1000);
                    }
                });

            })/*.fadeOut(1000)*/;

            /*$.ajax({
                url: '{{ route('Keeper.roles.module_actions', $id) }}',
                method: 'POST',
                data: {module_key: module_key_, _token: token},
                success: function (data) {
                    var loader_el = $("#loader-overlay",el);
                    if(loader_el.length){
                        $(".card-body", el).append(data);
                            $(loader_el).fadeOut(500, function(){
                                $(".card-body", el).animate(
                                  {'background-color':'#f3dc19'},300,
                                  function() {
                                      $(this).animate({'background-color': '#fff'}, 500,function(){
                                          $(loader_el).remove();
                                      });
                                  }
                                );
                            });
                    }else{
                        $(".card-body", el).html(data);
                    }
                    $(".accordion").accordion({
                        heightStyle: "content"
                    });
                    $('.checkbox').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue'
                    });
                },
                error: function (xhr, status, errorThrown) {
                    console.log(errorThrown);
                }
            });*/
        }

        $(document).ready(function () {
            $("html").delegate('div#accordion .card.subs-unloaded .card-header h5 button', 'click', function () {
                var module_key = $(this).closest(".card").data("module");
                $(this).closest(".card.subs-unloaded").removeClass("subs-unloaded");
                fillModuleSubs(module_key);

            });

            $("html").delegate('div#accordion .card-header h5 button', 'click', function () {
                var card_body = $(this).closest(".card").find(".card-body").first();
                $(card_body).children('.form-group').children(".checkbox").iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
            });
        });
    </script>
@endpush
