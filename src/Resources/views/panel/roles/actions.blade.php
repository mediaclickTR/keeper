{{--@php dump($actions) @endphp--}}
@php

global $_first_action, $dates;
$_first_action = true;
$dates=[];

    function renderActions($actionset, $entity, $abilities, $forbidden_abilities, $options=[]) {

        global $_first_action,$dates;
        if($_first_action){
            $dates["first"]=date("H:i:s", time());
        }
        $dates["last"]=date("H:i:s", time());

        $title_undefined = trans('KeeperPanel::roles.authorization.optionUndefined');
        $title_default = trans('KeeperPanel::roles.authorization.optionUseDefault');
        $title_allow = trans('KeeperPanel::roles.authorization.optionAllow');
        $title_deny = trans('KeeperPanel::roles.authorization.optionDeny');
        $forbidden = null; $allowed = null;
        $text= '<div id="'.$options["accordion_id"].'">';
        $icons = [
            "index"             =>  "list",
            "create"            =>  "plus-square-o",
            "update"            =>  "pencil-square-o",
            "edit_update"       =>  "pencil-square-o",
            "delete"            =>  "minus-square-o",
            "export"            =>  "share",
            "reorder"           =>  "sort",
            "view"              =>  "desktop",
            "mark_all_unread"   =>  "envelope-o"
         ];
        foreach($actionset as $setkey => $set){

        //dd($set);

            $name_affix = trans($set["name_affix"] ?? "");
            $title_undefined = array_get($set,'data.descendant_of_variation',false) || array_get($set,'data.is_variation',false) ?  $title_default : $title_undefined;

            $set["name"] =  trans($set["name"]);
            $set["slug"] = uniqid();
            $text.= '
                    <div class="card">
                        <div class="card-header" id="heading_'.$set["slug"].'">
                            <h5 class="mb-0">
                                <button type="button" class="btn btn-link'.( $_first_action ?"": " collapsed").'" data-toggle="collapse" data-target="#collapse_'.$set["slug"].'" aria-expanded="false" aria-controls="collapse_'.$set["slug"].'">'
                                        . $set["name"].($name_affix ? " - " . $name_affix : "").
                                '</button>
                            </h5>
                        </div>
                        <div id="collapse_'.$set["slug"].'" class="collapse'.( $_first_action ?" show": "").'" aria-labelledby="heading_'.$set["slug"].'" data-parent="#'. $options["accordion_id"] .'">
                            <div class="card-body">';

            if(count($set["actions"])){
                    foreach ($set["actions"] as $actkey => $action){
                        $barekey = last(explode('.',$actkey));
                        $icon = "fa-". (array_key_exists($barekey,$icons) ? $icons[$barekey]: "bullseye");
                        $forbidden = null; $allowed = null;
                        $forbidden = in_array($actkey, $forbidden_abilities) ? true : false;
                        if( ! $forbidden){
                            $allowed = in_array($actkey, $abilities) ? true : false;
                        }
                        //<i class="fa '. $icon .'"></i>
                        $text.='<div class="form-group"><span>'.trans($action['name']).'</span><div class="checkbox">';
                        $default_is_forbidden=true;
                        if(!($allowed || $forbidden)){
                            $default_is_forbidden=true;
                        }
                        if(array_get($set,'data.descendant_of_variation',false) || array_get($set,'data.is_variation',false)){
                            $default_is_forbidden = false;
                            $text.='<label for="'.$actkey.'-null'.'">'.$title_undefined.'<input type="radio"  id="'.$actkey.'-null'.'"  name="actions['.$actkey.']" value="" '.(!($allowed || $forbidden) ? "checked" : "").'></label>';
                        }



                        $text .= '<label for="'.$actkey.'-allow'.'" class="active">'.$title_allow.'<input type="radio" id="'.$actkey.'-allow'.'"  name="actions['.$actkey.']" value="allow" '.($allowed ? "checked" : "").'></label>
                          <label for="'.$actkey.'-deny'.'" class="passive">'.$title_deny.'<input type="radio" id="'.$actkey.'-deny'.'"  name="actions['.$actkey.']" value="deny" '.($forbidden || $default_is_forbidden ? "checked" : "").'></label>';
                        $text .= '</div></div>';
                    }
            }

            $_first_action = false;

            if(count($set['subs'])){
                $id = uniqid("accordion_");
                $text.= /*'<h3 class="name">'. trans("KeeperPanel::".'roles.app.sub_modules') .'</h3>'.*/renderActions($set['subs'],$entity,$abilities,$forbidden_abilities,["accordion_id"=>$id]);
            }

            if(count($set['variations'])){
                $id = uniqid("accordion_");
                $title = trans($set["variations_title"] ?? "MPCorePanel::auth.general.recorded_items");//isset($set["variations_title"]) ? trans($set["variations_title"]) : trans("KeeperPanel::".'roles.app.recorded_items') .' - '. $set["name"];
                $text.= '<h3 class="name">'. $title.'</h3>'.renderActions($set['variations'], $entity, $abilities, $forbidden_abilities,["accordion_id"=>$id]);
            }

            $text .= "</div></div></div>";
        }
        return $text.'</div>';
    }

    $forbidden_abilities = $role->getForbiddenAbilities()->pluck("name")->toArray();
    $abilities = $role->getAbilities(false)->pluck("name")->toArray();
    $forbidden = null; $allowed = null;
    $forbidden = in_array("*", $forbidden_abilities) ? true : false;

    if( ! $forbidden){
        $allowed = in_array("*", $abilities) || $role->can("*") ? true : false;
    }
@endphp
{!! renderActions($actions["actions"], $role,$abilities,$forbidden_abilities,["accordion_id"=>uniqid("accordion_")]) !!}
{!! renderActions($actions["subs"], $role,$abilities,$forbidden_abilities,["accordion_id"=>uniqid("accordion_")]) !!}
{!! renderActions($actions["variations"], $role,$abilities,$forbidden_abilities,["accordion_id"=>uniqid("accordion_")]) !!}
{{--@php dd($actions) @endphp--}}
@dump($dates)
