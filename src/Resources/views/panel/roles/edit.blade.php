@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="content">
            @include("MPCorePanel::inc.errors")
            <h1>{!! trans("KeeperPanel::roles.edit") !!}</h1>
            @include("KeeperPanel::roles.form")
        </div>
    </div>
@endsection
