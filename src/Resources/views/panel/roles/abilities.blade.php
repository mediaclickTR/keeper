@extends("MPCorePanel::inc.app")
@push("styles")
    <link rel="stylesheet" href="{!! asset("/vendor/mediapress/css/selector.css") !!}"/>
    <link rel="stylesheet" href="{!! asset("/vendor/mediapress/css/jquery/jquery-ui.css") !!}">
@endpush

@php
    function renderActions($actionset, $entity, $abilities, $forbidden_abilities, $options=[]) {
        $title_undefined = trans('KeeperPanel::roles.authorization.optionUndefined');
        $title_default = trans('KeeperPanel::roles.authorization.optionUseDefault');
        $title_allow = trans('KeeperPanel::roles.authorization.optionAllow');
        $title_deny = trans('KeeperPanel::roles.authorization.optionDeny');
        $forbidden = null; $allowed = null;
        $text= '<div id="'.$options["accordion_id"].'">';
        $icons = [
            "index"             =>  "list",
            "create"            =>  "plus-square-o",
            "update"            =>  "pencil-square-o",
            "edit_update"       =>  "pencil-square-o",
            "delete"            =>  "minus-square-o",
            "export"            =>  "share",
            "reorder"           =>  "sort",
            "view"              =>  "desktop",
            "mark_all_unread"   =>  "envelope-o"
         ];
        foreach($actionset as $setkey => $set){

            $name_affix = array_get($set,'name_affix',null);
            $title_undefined = array_get($set,'data.descendant_of_variation',false) || array_get($set,'data.is_variation',false) ?  $title_default : $title_undefined;

            $set["name"] =  trans($set["name"]);
            $set["slug"] = uniqid();
            $text.= '
                    <div class="card">
                        <div class="card-header" id="heading_'.$set["slug"].'">
                            <h5 class="mb-0">
                                <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_'.$set["slug"].'" aria-expanded="false" aria-controls="collapse_'.$set["slug"].'">'
                                        . $set["name"].($name_affix ? " - " . $name_affix : "").
                                '</button>
                            </h5>
                        </div>
                        <div id="collapse_'.$set["slug"].'" class="collapse" aria-labelledby="heading_'.$set["slug"].'" data-parent="#'. $options["accordion_id"] .'">
                            <div class="card-body">';

            if(count($set["actions"])){
                    foreach ($set["actions"] as $actkey => $action){
                        $barekey = last(explode('.',$actkey));
                        $icon = "fa-". (array_key_exists($barekey,$icons) ? $icons[$barekey]: "bullseye");
                        $forbidden = null; $allowed = null;
                        $forbidden = in_array($actkey, $forbidden_abilities) ? true : false;
                        if( ! $forbidden){
                            $allowed = in_array($actkey, $abilities) ? true : false;
                        }
                        //<i class="fa '. $icon .'"></i>
                        $text.='<div class="form-group"><span>'.trans("KeeperPanel::roles.header.".$action['name']).'</span><div class="checkbox">';
                        $text .= ( (array_get($set,'data.descendant_of_variation',false) || array_get($set,'data.is_variation',false)) ?
                        '<label for="'.$actkey.'-null'.'">'.$title_undefined.'<input type="radio"  id="'.$actkey.'-null'.'"  name="actions['.$actkey.']" value="" '.(!($allowed || $forbidden) ? "checked" : "").'></label>':"")
                        .'<label for="'.$actkey.'-allow'.'" class="active">'.$title_allow.'<input type="radio" id="'.$actkey.'-allow'.'"  name="actions['.$actkey.']" value="allow" '.($allowed ? "checked" : "").'></label>
                          <label for="'.$actkey.'-deny'.'" class="passive">'.$title_deny.'<input type="radio" id="'.$actkey.'-deny'.'"  name="actions['.$actkey.']" value="deny" '.($forbidden ? "checked" : "").'></label>';
                        $text .= '</div></div>';
                    }
            }

            if(count($set['subs'])){
                $id = uniqid("accordion_");
                $text.= '<h3>'. trans("KeeperPanel::".'roles.app.sub_module') .' - '. $set["name"].'</h3>'.renderActions($set['subs'],$entity,$abilities,$forbidden_abilities,["accordion_id"=>$id]);
            }

            if(count($set['variations'])){
                $id = uniqid("accordion_");
                $text.= '<h3>'. trans("KeeperPanel::".'roles.app.variation') .' - '. $set["name"].'</h3>'.renderActions($set['variations'], $entity, $abilities, $forbidden_abilities,["accordion_id"=>$id]);
            }

            $text .= "</div></div></div>";
        }
        return $text.'</div>';
    }

    $forbidden_abilities = $role->getForbiddenAbilities()->pluck("name")->toArray();
    $abilities = $role->getAbilities(false)->pluck("name")->toArray();
    $forbidden = null; $allowed = null;
    $forbidden = in_array("*", $forbidden_abilities) ? true : false;

    if( ! $forbidden){
        $allowed = in_array("*", $abilities) || $role->can("*") ? true : false;
    }
@endphp


@push('scripts')
    <script src="{!! asset("/vendor/mediapress/js/jquery-ui.js") !!}"></script>
    <script>
        $(function () {
            $('[name=mode]').on('ifChecked', function () {
                var mode = $(this).val();
                switch (mode) {
                    case "everything":
                    case "nothing":
                        $('div#accordion').slideUp(200);
                        break;
                    case "custom":
                        $('div#accordion').slideDown(200);
                        break;
                    default:
                        break;
                }
            });
            $('[name=mode]:checked').trigger('ifChecked');
        });
    </script>
@endpush

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{{ $title }} - {{ $role->title }} ({{ $role->name }})</div>
        {{--<a href="{{route("Keeper.roles.index")}}" class="btn btn-primary"><i class="fa fa-arrow-left"></i> {{trans("MPCorePanel::general.back")}}</a>--}}
        {{Form::open(["class" => "form-horizontal", "id" => "saveAbilities", "autocomplete" => "off"])}}
        <div class="form-group">
            <div class="checkbox">
                <label class="active">
                    <input type="radio" name="mode"
                           value="everything" {!! $allowed ? "checked" : "" !!}>
                    {{trans('KeeperPanel::roles.ability.allow')}}
                </label>
                <label class="passive">
                    <input type="radio" name="mode" value="nothing" {!! $forbidden ? "checked" : "" !!}>
                    {{trans('KeeperPanel::roles.ability.no_permission')}}
                </label>
                <label>
                    <input type="radio" name="mode"
                           value="custom" {!! (!($allowed || $forbidden) ? "checked" : "") !!}>
                    {{trans('KeeperPanel::roles.ability.special')}}
                </label>
            </div>
        </div>
        <div id="accordion">
            {!! renderActions($actions, $role,$abilities,$forbidden_abilities,["accordion_id"=>uniqid("accordion_")]) !!}
        </div>
        <div class="float-right">
            <button class="btn btn-primary">{{trans('MPCorePanel::general.save')}}</button>
        </div>
        {{Form::close()}}
    </div>
@endsection
