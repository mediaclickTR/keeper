@extends('MPCorePanel::inc.app')

@section("content")
    <div class="page-content">
        <div class="widget">
            <div class="widget-heading">
                <h3 class="widget-title"><i class="fa fa-cluster"></i>
                    {{trans("KeeperPanel::roles.titles.index")}}
                    @if(userAction('role.create',true,false))
                        <a href="{{route("Keeper.roles.create")}}" class="btn btn-primary float-right">
                            <i class="fa fa-plus-circle"></i> {{trans('KeeperPanel::roles.add_new')}}
                        </a>
                    @endif
                </h3>
            </div>
            <div class="widget-body pt-3">
                <div class="table-responsive mt-2">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
