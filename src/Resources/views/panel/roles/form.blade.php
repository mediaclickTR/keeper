@if(!isset($role))
    {{ Form::open(['route' => route('Keeper.roles.update',$role)]) }}
@else
    {{ Form::model($role, ['route' => ['Keeper.roles.update',$role], 'method' => 'POST']) }}
@endif
@csrf

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::roles.slug") !!}</label>
    {!!Form::text('name', null, ["placeholder"=>trans("KeeperPanel::roles.slug"), "class"=>"validate[required]"])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::roles.title") !!}</label>
    {!!Form::text('title', null, ["placeholder"=>trans("KeeperPanel::roles.title"), "class"=>"validate[required]"])!!}
</div>

<div class="float-right">
    @if($role->status == 3)
        <button class="btn btn-primary">{!! trans("KeeperPanel::roles.save_and_edit_perm") !!}</button>
    @else
        <a class="btn btn-primary" href="{{route("Keeper.roles.abilities",$role)}}">{!! trans("KeeperPanel::roles.edit_perm") !!}</a>
        <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
    @endif
</div>


