@if(!isset($admin))
    {{ Form::open(['route' => 'Keeper.admins.store']) }}
@else
    {{ Form::model($admin, ['route' => ['Keeper.admins.update'], 'method' => 'POST']) }}
    {!! Form::hidden("id",$admin->id) !!}
@endif
@csrf

<div class="form-group">
    <div class="tit">{!! trans("KeeperPanel::admin.roles") !!}</div>
    {!!Form::select('role_id', $roles,null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => 'nice'])!!}
</div>

<div class="form-group">
    <div class="tit">{!! trans("KeeperPanel::admin.language") !!}</div>
    {!!Form::select('language_id', $languages,null, ['placeholder' => trans("MPCorePanel::general.selection"),'class' => 'nice'])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::admin.name-admin") !!}</label>
    {!!Form::text('first_name', null, ["placeholder"=>trans("KeeperPanel::admin.name-admin"), "class"=>"validate[required]"])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::admin.surname-admin") !!}</label>
    {!!Form::text('last_name', null, ["placeholder"=>trans("KeeperPanel::admin.surname-admin")])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::admin.username-admin") !!}</label>
    {!!Form::text('username', null, ["placeholder"=>trans("KeeperPanel::admin.username-admin")])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::admin.email-admin") !!}</label>
    {!!Form::text('email', null, ["placeholder"=>trans("KeeperPanel::admin.email-admin"), "class"=>"validate[required]"])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::admin.phone-admin") !!}</label>
    {!!Form::text('phone', null, ["placeholder"=>trans("KeeperPanel::admin.phone-admin")])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::admin.password-admin") !!}</label>
    {!!Form::password('password', null, ["placeholder"=>trans("KeeperPanel::admin.password-admin")])!!}
</div>

<div class="form-group rel focus">
    <label>{!! trans("KeeperPanel::admin.repeat-password-admin") !!}</label>
    {!!Form::password('password_confirmation', null, ["placeholder"=>trans("KeeperPanel::admin.repeat-password-admin")])!!}
</div>
<div class="float-right">
    <button class="btn btn-primary">{!! trans("MPCorePanel::general.save") !!}</button>
</div>
