@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <h1 class="title">{!! trans("KeeperPanel::admin.edit-admin") !!}</h1>
        <div class="col-md-12">
            @include("KeeperPanel::admins.form")
        </div>
    </div>
@endsection
