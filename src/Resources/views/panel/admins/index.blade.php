@extends('MPCorePanel::inc.app')
@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        @include("MPCorePanel::inc.errors")
        <div class="title">{!! trans("KeeperPanel::admin.admins") !!}</div>
            <div class="float-right">
                <a class="btn btn-primary" href="{!! route('Keeper.admins.create') !!}"> <i class="fa fa-plus"></i> {!! trans("MPCorePanel::general.new_add") !!}</a>
            </div>
              <div class="clearfix"></div>
            <div class="table-field">
                {!! $dataTable->table() !!}
            </div>
    </div>
@endsection
