<?php

return [
    "edit" => "Rol Düzenle",
    "slug" => "Rol Slug'ı",
    "title" => "Rol Başlığı",
    "save_and_edit_perm" => "Kaydet ve Yetkileri Düzenle",
    "edit_perm" => "Yetkileri Düzenle",
    "add_new" => "Yeni Ekle",
    'ability' => [
        'allow' => 'Tüm izinleri ver',
        'no_permission' => 'Hiç izin verme',
        'special' => 'Özel'
    ],
    "header" => [
        "Listele" => "Listele",
        "Oluştur" => "Oluştur",
        "Düzenle" => "Düzenle",
        "Sil" => "Sil",
        "Görüntüle" => "Görüntüle",
        "Okunmadı Olarak İşaretle" => "Okunmadı Olarak İşaretle",
        "Excel'e Aktar" => "Excel'e Aktar",
        "Excele Aktar" => "Excele Aktar",
        "Ekle" => "Ekle",
        "Güncelle" => "Güncelle",
        "Formu Güncelle" => "Formu Güncelle",
        "Sırala" => "Sırala",
    ],
    "app" => [
        "sub_module" => "Alt Bileşen",
        "sub_modules" => "Alt Bileşenler",
        "module" => "Bileşen",
        "variation" => "Varyasyon",
        "recorded_items"=>"Kayıtlı Öğeler"
    ],
    "titles" => [
        "index" => "Roller",
        "Ortak Yetkiler" => "Ortak Yetkiler",
        "Sayfa Yapısı" => "Sayfa Yapısı",
        "Cluster Detayı" => "Cluster Detayı",
        "Sayfa" => "Sayfa",
        "Kategori" => "Kategori",
        "Kriter" => "Kriter",
        "Özellik" => "Özellik",
        "Mesajlar" => "Mesajlar",
        "SEO" => "SEO",
        "Rol" => "Rol",
        "Menüler" => "Menüler",
    ],
    "authorization" => [
        'optionAllow' => "İzin ver",
        'optionDeny' => "Yasakla",
        'optionUseDefault' => "Varsayılan",
    ]
];
