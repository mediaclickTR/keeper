<?php

return [
    'admins' => 'Yöneticiler',
    'admin_accounts' => 'Yönetici Hesapları',
    'roles' => 'Roller',
    'permissions' => 'Yetkiler'
];
