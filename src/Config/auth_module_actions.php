<?php

return [
    "admins" => [
        'name' => 'KeeperPanel::auth.sections.admins',
        'item_model' => null,
        'actions' => [
            'index' => [
                'name' => "MPCorePanel::auth.actions.list",
            ],
            'create' => [
                'name' => "MPCorePanel::auth.actions.create",
            ],
            'update' => [
                'name' => "MPCorePanel::auth.actions.update"
            ],
            'delete' => [
                'name' => "MPCorePanel::auth.actions.delete",
            ]
        ],
    ],
    "roles" => [
        'name' => 'KeeperPanel::auth.sections.roles',
        'item_model' => null,
        'actions' => [
            'index' => [
                'name' => "MPCorePanel::auth.actions.list",
            ],
            'create' => [
                'name' => "MPCorePanel::auth.actions.create",
            ],
            'update' => [
                'name' => "MPCorePanel::auth.actions.update"
            ],
            'delete' => [
                'name' => "MPCorePanel::auth.actions.delete",
            ]
        ],

            "subs" => [
                "abilities" => [
                    'name' => 'KeeperPanel::auth.sections.abilities',
                    'item_model' => null,
                    'data' => [
                        "is_root" => false,
                        "is_variation" => false,
                        "is_sub" => true,
                        "descendant_of_sub" => false,
                        "descendant_of_variation" => false
                    ],
                    'actions' => [
                        'index' => [
                            'name' => "MPCorePanel::auth.actions.list",
                            'variables' => [],
                        ],
                        'update' => [
                            'name' => "MPCorePanel::auth.actions.update"
                        ]
                    ],
                ],
            ]

    ],
];
