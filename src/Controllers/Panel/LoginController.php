<?php

namespace Mediapress\Keeper\Controllers\Panel;

use anlutro\cURL\cURL;
use Mediapress\Models\Admin;
use Mediapress\Keeper\Models\Administrator;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Keeper\Models\Role;
use Silber\Bouncer\BouncerFacade as Bouncer;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    public const ADMIN = 'admin';
    public const URL_INTENDED = 'url.intended';
    public const EMAIL = 'email';
    public const SUPER_MC_ADMIN = 'SuperMCAdmin';
    public const VALUES = 'values';
    protected $guard = self::ADMIN;

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/mp-admin';

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        Parent::__construct();
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        if (!session()->has(self::URL_INTENDED)) {
            session([self::URL_INTENDED => url()->previous()]);
        }
        // Website ve user session'ı oluşturulduysa dashboard'a yönlendir
        if (session("panel.website") && session("panel.user")) {
            return redirect()->route("panel.dashboard");
        }

        app()->setLocale("tr");

        return view('KeeperPanel::login');
    }


    public function login(Request $request)
    {


        $array = $request->all();
        unset($array["_token"]);
        $attempt = Auth::guard($this->guard)->attempt($array);
        if ($attempt) {
            $response = Admin::where(self::EMAIL, $request->email)->firstOrFail();
            if ($response) {
                $admin = Admin::find($response->id);
                // Website Session oluşturuluyor
                $this->setWebsiteSession();
                // Panel languages session oluşturuluyor
                $this->setPanelLanguagesSession();
                // Panel active language session oluşturuluyor
                $this->setPanelActiveLanguageSession();
                // user_id Session oluşturuluyor
                $this->setUserSession($admin);

                 Auth::guard(self::ADMIN)->login($admin, true);

                // Log
                if (session()->has('panel.website') && session()->has('panel.user')) {
                    UserActionLog::login(__CLASS__ . "@" . __FUNCTION__, $admin);
                }

                return redirect()->to(session()->get(self::URL_INTENDED));
            }
        }
        return redirect()->back()->withErrors(trans('AuthPanel::auth.failed'));

    }

    public function logout(Request $request)
    {
        $session_user = Auth::guard(self::ADMIN)->user();
        // Log
        UserActionLog::logout(__CLASS__ . "@" . __FUNCTION__, $session_user);

        Auth::guard(self::ADMIN)->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect(panel('/'));
    }
}
