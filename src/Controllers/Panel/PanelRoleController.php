<?php

namespace Mediapress\Keeper\Controllers\Panel;

use App\Http\Controllers\Controller;
use Html;
use Mediapress\DataTable\TableBuilderTrait;
use Mediapress\Facades\ModulesEngine;
use Mediapress\Foundation\MPCache;
use Mediapress\Models\Url;
use Mediapress\Models\Image;
use Mediapress\Keeper\Models\Role;
use Mediapress\Keeper\Models\Role as BRole;
use Mediapress\Modules\MPCore\Facades\MPCore;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Collection;
use Kris\LaravelFormBuilder\FormBuilder;
use Mediapress\FormBuilder\FormStore;
use Mediapress\FormBuilder\PanelFormBuilder;
use Mediapress\Models\Menu;
use Mediapress\Models\MenuDetail;
use Mediapress\Models\LanguageWebsite;
use Silber\Bouncer\BouncerFacade as Bouncer;
use File;
use Validator;
use Cache;
use DB;

class PanelRoleController extends Controller
{
    use TableBuilderTrait;

    public const TITLE = "title";
    public const ACCESSDENIED = 'accessdenied';
    public const ACTIONS = 'actions';

    public function __construct()
    {
        Bouncer::useRoleModel(BRole::class);
    }

    public function index(Builder $builder)
    {
        if (!userAction('role.index', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }
        $dataTable = $this->columns($builder)->ajax(route('Keeper.roles.ajax'));
        //return dd($dataTable);

        return view("KeeperPanel::roles.index", compact("cluster", "dataTable"));
    }


    /**
     * @param FormBuilder $formBuilder
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function formCreate()
    {
        if (!userAction('Keeper.roles.create', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        if ($create = Role::preCreate()) {
            return redirect(route("Keeper.roles.edit", $create->id));
        } else {
            throw new \Exception("Rol Modeli Oluşturulamadı : " . json_encode($create));
        }
    }


    public function edit(Role $role)
    {
        return view("KeeperPanel::roles.edit", compact("role"));
    }

    public function update(Role $role)
    {
        $redirect_permission_page = $role->status == 3;

        try {
            $role->title = \request()->get(self::TITLE);
            $role->name = \request()->get("name");
            $role->status = 1;
            $role->save();
        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }

        if ($redirect_permission_page) {
            return redirect()->to(route("Keeper.roles.abilities", $role));
        } else {
            return redirect()->to(route("Keeper.roles.index"));
        }
    }

    public function abilities2(Request $request, $id = 0)
    {
        $role = BRole::find($id);

        if (!$role) {
            abort(404);
        }

        $title = "Yetkiler";

            $module_name = null;
            $modules=ModulesEngine::getMPModules(true);
            $modules_arr = [];
            if($request->has("module_name") && $request->module_name && in_array(array_keys($modules))){
                $modules_arr= [$request->module_name];
            }
            $headers_only = true;
            if($request->has("mode") && $request->mode == "module_actions"){
                $headers_only = false;
            }


            $actions =ModulesEngine::getModulesActions($modules_arr,$headers_only);



            return view("KeeperPanel::roles.abilities2", compact("id", "role", "abilities", self::ACTIONS, self::TITLE, "button" ,"actions"));

            //return dd($actions);


        //$actions = getActions(null, true, true);
        /*return $actions;

        $abilities = $role->getAbilities();
        $forbidden = $role->getForbiddenAbilities();

        $title = "Yetkiler";


        return view("KeeperPanel::roles.abilities2", compact("id", "role", "abilities", "forbidden", self::ACTIONS, self::TITLE, "button"));*/

    }

    public function ajaxModuleActions(Request $request,  $id){

        $role = BRole::find($id);

        if (!$role) {
            abort(404);
        }

        $modules=ModulesEngine::getMPModules();
        if($request->has("module_key") && $request->module_key && in_array($request->module_key,array_keys($modules))){
            $module_key= $request->module_key;
        }

        $actions =ModulesEngine::getModuleActions($module_key,$modules[$module_key], false);

        if( ! (count($actions["actions"]) || count($actions["subs"]) || count($actions["variations"]))){
            return '<br><div><p class="text-center">Bu modüle ait herhangi bir eylem tanımı bulunmamaktadır...</p></div>';
        }

        return view("KeeperPanel::roles.actions", compact('actions','role'));

    }

    public function storeAbilities(Request $request, $id = 0){

        $role = BRole::find($id);

        if (!$role) {
            abort(404);
        }
        //todo:yarım kaldı sıfırdan yaz tamamla:
        $data = $request->input()['actions'];
        $mode = $request->input()['mode'];
        //dd($mode);
        if ($mode == "everything") {
            //dump("in everything");
            Bouncer::unforbid($role->name)->everything();
            //dump("did unforbid everything");
            Bouncer::allow($role->name)->everything();
            //dump("did allow everything");
            data_set($data, '*', null);
        } elseif ($mode == "nothing") {
            Bouncer::disallow($role->name)->everything();
            Bouncer::forbid($role->name)->everything();
            data_set($data, '*', null);
        } elseif ($mode == "custom") {
            Bouncer::disallow($role->name)->everything();
            Bouncer::unforbid($role->name)->everything();
        }
        foreach ($data as $actkey => $action) {
            switch ($action) {
                case "allow":
                    $role->unforbid($actkey);
                    $role->allow($actkey);
                    break;
                case "deny":
                    $role->disallow($actkey);
                    $role->forbid($actkey);
                    break;
                case "":
                    $role->disallow($actkey);
                    $role->unforbid($actkey);
                    break;
            }
        }
        //Cache::forget(cache_key('mainMenu'));
        return redirect()->to(route("Keeper.roles.abilities", $id));
    }

    /**
     * @param FormBuilder $formBuilder
     * @param Collection $data
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function save(FormBuilder $formBuilder, Collection $data)
    {
        if (!userAction('role.update', true, false)) {
            return [
                false, trans("panel::general.ajax.access_error")
            ];
        }

        $id = $data->pull("parameters.id");

        $role = BRole::find($id);

        if ($role) {
            $role = array_first(FormStore::store($formBuilder, "role", "default", [], [
                "find" => ["id", "=", $id], "data" => $data->except("parameters")->toArray()
            ]));
        } else {
            $role = array_first(FormStore::store($formBuilder, "role", "default", [], [
                "data" => $data->except("parameters")->toArray()
            ]));
        }

        if ($role) {
            return [
                true, trans("panel::general.ajax.create_success", ["name" => $role->title])
            ];
        } else{
            return [
                false, trans("panel::general.ajax.create_error", ["name" => $role->title])
            ];
        }
    }


    /**
     * @param Collection $data
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        if (!userAction('role.delete', true, false)) {
            return redirect()->to(url(route(self::ACCESSDENIED)));
        }

        if (!$id){
            return redirect()->back()->withErrors(trans("panel::roles.crud.no_record"));
        }

        $role = BRole::find($id);

        if (!$role){
            return redirect()->back()->withErrors(trans("panel::roles.crud.no_record"));
        }
        if ($role->delete()){
            return redirect()->back();
        }

        return [false, trans("panel::roles.crud.delete_failed", ["name" => $role->title])];
    }
}
