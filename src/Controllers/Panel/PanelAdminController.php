<?php

namespace Mediapress\Keeper\Controllers\Panel;

use Mediapress\Models\Admin;
use Mediapress\Keeper\Models\Administrator;
use Mediapress\Keeper\Models\Role;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\DataTable\TableBuilderTrait;
use Yajra\DataTables\Html\Builder;
use Illuminate\Http\Request;
use Mediapress\Modules\MPCore\Facades\MPCore;

class PanelAdminController extends Controller
{
    use TableBuilderTrait;

    public const ADMIN = "admin";
    public const ROLE_ID = 'role_id';
    public const LANGUAGE_ID = 'language_id';
    public const FIRST_NAME = 'first_name';
    public const LAST_NAME = 'last_name';
    public const USERNAME = 'username';
    public const EMAIL = 'email';
    public const PASS_WORD = 'password';
    public const KEEPER_PANEL_ADMIN_EMAIL_ADMIN = "KeeperPanel::admin.email-admin";
    public const KEEPER_PANEL_ADMIN_PASS_WORD_ADMIN = "KeeperPanel::admin.password-admin";
    public const KEEPER_PANEL_ADMIN_USERNAME_ADMIN = "KeeperPanel::admin.username-admin";
    public const KEEPER_PANEL_ADMIN_LAST_NAME_ADMIN = "KeeperPanel::admin.last-name-admin";
    public const KEEPER_PANEL_ADMIN_NAME_ADMIN = "KeeperPanel::admin.name-admin";
    public const KEEPER_PANEL_ADMIN_ROLE_ADMIN = "KeeperPanel::admin.role-admin";
    public const REQUIRED = 'required';
    public const MP_CORE_PANEL_VALIDATION_FILLED = "MPCorePanel::validation.filled";
    public const FILLED = 'filled';
    public const CONFIRMED = 'confirmed';
    public const KEEPER_ADMINS_INDEX = "Keeper.admins.index";
    public const MESSAGE = 'message';
    public const MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE = 'MPCorePanel::general.success_message';
    public const ERROR = 'error';
    public const MP_CORE_PANEL_GENERAL_ERROR_MESSAGE = 'MPCorePanel::general.error_message';

    public function __construct()
    {
        Bouncer::useRoleModel(Role::class);
    }

    public function index(Builder $builder)
    {
        if(!userAction('admin.index',true,false)){
            return redirect()->to(url(route('accessdenied')));
        }
        $dataTable = $this->columns($builder)->ajax(route('Keeper.admins.ajax'));

        return view("KeeperPanel::admins.index", compact('dataTable'));
    }

    public function create()
    {
        $languages = MPCore::getLanguages()->pluck("name", "id");
        $roles = Role::get()->pluck("name", "id");
        $form = "formbuilderden gelecek";

        return view("KeeperPanel::admins.create", compact("roles","languages","id", "form", "title", "button", self::ADMIN));
    }

    public function edit($id)
    {
        $admin = Administrator::find($id);
        $languages = MPCore::getLanguages()->pluck("name", "id");
        $roles = Role::get()->pluck("name", "id");
        $form = "formbuilderden gelecek";

        return view("KeeperPanel::admins.create", compact(self::ADMIN,"roles","languages","id", "form", "title", "button", self::ADMIN));
    }

    public function store(Request $request)
    {
        $roles_to_assign[] = $request->role_id;

        Bouncer::useRoleModel(Role::class);

        /*
         * Validation
         */

        $fields = [
            self::ROLE_ID => trans(self::KEEPER_PANEL_ADMIN_ROLE_ADMIN),
            self::LANGUAGE_ID => trans(self::KEEPER_PANEL_ADMIN_ROLE_ADMIN),
            self::FIRST_NAME => trans(self::KEEPER_PANEL_ADMIN_NAME_ADMIN),
            self::LAST_NAME => trans(self::KEEPER_PANEL_ADMIN_LAST_NAME_ADMIN),
            self::USERNAME => trans(self::KEEPER_PANEL_ADMIN_USERNAME_ADMIN),
            'phone' => trans("KeeperPanel::admin.phone-admin"),
            self::EMAIL => trans(self::KEEPER_PANEL_ADMIN_EMAIL_ADMIN),
            self::PASS_WORD => trans(self::KEEPER_PANEL_ADMIN_PASS_WORD_ADMIN),
        ];

        $rules = [
            //'role_id' => 'required',
            self::LANGUAGE_ID => self::REQUIRED,
            self::FIRST_NAME => self::REQUIRED,
            self::LAST_NAME => self::REQUIRED,
            self::USERNAME => self::REQUIRED,
            self::EMAIL => 'required|unique:admins',
            self::PASS_WORD => 'required|confirmed',
        ];

        $messages = [
            'role_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_ROLE_ADMIN)]),
            'language_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans("KeeperPanel::admin.language-admin")]),
            'first_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_NAME_ADMIN)]),
            'last_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_LAST_NAME_ADMIN)]),
            'username.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_USERNAME_ADMIN)]),
            'email.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_EMAIL_ADMIN)]),
            'password.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_PASS_WORD_ADMIN)]),
            'password.confirmed' => trans("MPCorePanel::validation.confirmed", [self::CONFIRMED,trans(self::KEEPER_PANEL_ADMIN_PASS_WORD_ADMIN)]),
            'email.unique' =>  trans("MPCorePanel::validation.unique", ['unique',trans(self::KEEPER_PANEL_ADMIN_EMAIL_ADMIN)]),
        ];


        $data = $request->except("_token","password_confirmation", self::PASS_WORD);
        $data += [self::PASS_WORD =>bcrypt($request->password)];

        $this->validate($request, $rules, $messages, $fields);

        $insert = Administrator::firstOrCreate($data);

        if ($insert){

            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            if($schema->hasColumn('admins', 'api_token')) {
                $insert->api_token = uniqid();
                $insert->save();
            }

            // assign requested roles
            foreach($roles_to_assign as $rta){
                Bouncer::assign($rta)->to($insert);
            }
            // Log
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$insert);

            return redirect()->route(self::KEEPER_ADMINS_INDEX)->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_ERROR_MESSAGE));
    }

    public function update(Request $request)
    {
        $admin = Administrator::find($request->id);
        $roles_to_assign[] = $request->role_id;
        Bouncer::useRoleModel(Role::class);

        /*
         * Validation
         */

        $fields = [
            self::ROLE_ID => trans(self::KEEPER_PANEL_ADMIN_ROLE_ADMIN),
            self::LANGUAGE_ID => trans(self::KEEPER_PANEL_ADMIN_ROLE_ADMIN),
            self::FIRST_NAME => trans(self::KEEPER_PANEL_ADMIN_NAME_ADMIN),
            self::LAST_NAME => trans(self::KEEPER_PANEL_ADMIN_LAST_NAME_ADMIN),
            self::USERNAME => trans(self::KEEPER_PANEL_ADMIN_USERNAME_ADMIN),
            'phone' => trans("KeeperPanel::admin.phone-admin"),
            self::EMAIL => trans(self::KEEPER_PANEL_ADMIN_EMAIL_ADMIN),
            self::PASS_WORD => trans(self::KEEPER_PANEL_ADMIN_PASS_WORD_ADMIN),
        ];

        $rules = [
            self::ROLE_ID => self::REQUIRED,
            self::LANGUAGE_ID => self::REQUIRED,
            self::FIRST_NAME => self::REQUIRED,
            self::LAST_NAME => self::REQUIRED,
            self::USERNAME => self::REQUIRED,
            self::EMAIL => 'unique:admins,email,'.$admin->id,
            self::PASS_WORD => self::CONFIRMED,
        ];

        $messages = [
            'role_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_ROLE_ADMIN)]),
            'language_id.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans("KeeperPanel::admin.language-admin")]),
            'first_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_NAME_ADMIN)]),
            'last_name.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_LAST_NAME_ADMIN)]),
            'username.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_USERNAME_ADMIN)]),
            'email.required' => trans(self::MP_CORE_PANEL_VALIDATION_FILLED, [self::FILLED,trans(self::KEEPER_PANEL_ADMIN_EMAIL_ADMIN)]),
            'password.confirmed' => trans("MPCorePanel::validation.confirmed", [self::CONFIRMED,trans(self::KEEPER_PANEL_ADMIN_PASS_WORD_ADMIN)]),
            'email.unique' =>  trans("MPCorePanel::validation.unique", ['unique',trans(self::KEEPER_PANEL_ADMIN_EMAIL_ADMIN)]),
        ];


        $data = $request->except("_token","password_confirmation", self::PASS_WORD);
        if ($request->password) {
            $data += [self::PASS_WORD =>bcrypt($request->password)];
        }

        $this->validate($request, $rules, $messages, $fields);

        $update=Administrator::updateOrCreate(['id'=>$admin->id],$data);

        if ($update){

            $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
            if($schema->hasColumn('admins', 'api_token')) {
                if(is_null($update->api_token)) {
                    $update->api_token = uniqid();
                    $update->save();
                }
            }
            $roles_assigned = $admin->roles()->get()->pluck('name')->toArray();
            // first retract current roles from user
            foreach($roles_assigned as $ra){
                $admin->retract($ra);
            }

            // assign requested roles
            foreach($roles_to_assign as $rta){
                Bouncer::assign($rta)->to($update);
            }
            // Log
            UserActionLog::create(__CLASS__."@".__FUNCTION__,$update);

            return redirect()->route(self::KEEPER_ADMINS_INDEX)->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_ERROR_MESSAGE));

    }

    public function delete($id)
    {
        if(!userAction('admin.delete',true,false)) {
            return redirect()->to(url(route('accessdenied')));
        }

        $admin = Admin::findOrFail($id);

        if ($admin->delete()) {
            return redirect()->route(self::KEEPER_ADMINS_INDEX)->with(self::MESSAGE, trans(self::MP_CORE_PANEL_GENERAL_SUCCESS_MESSAGE));
        }

        return redirect()->back()->with(self::ERROR, trans(self::MP_CORE_PANEL_GENERAL_ERROR_MESSAGE));
    }
}
