<?php

namespace Mediapress\Keeper\Controllers\Web;

use Mediapress\Foundation\Mediapress;
use Mediapress\Foundation\MediapressBasic;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Models\User;
use Mediapress\Models\UserExtra;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        login as traitLogin;
    }


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        if(Auth::check()){
            return $this->redirectTo;
        }

        return view("web.auth.login");
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        if($request->has('next')){
            $this->redirectTo = $request->get('next');
        }

       return  $this->traitLogin($request);
    }

    /**
     * Redirect the user to the Linkedin authentication page.
     *
     * @param $provider
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        $request = Request::capture();
        $referer = $request->server('HTTP_REFERER');
        session(['redirect'=>$referer]);
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Linkedin.
     *
     * @param $provider
     * @param Request $request
     * @return Response
     */
    public function handleProviderCallback($provider,Request $request)
    {
        if(!$request->has('code')){
            return redirect(route('user.login'));
        }

        $request->session()->put('state',$request->state);
        $provided_user = Socialite::driver($provider)->user();

        $user = User::updateOrCreate(
            [ 'email'=>$provided_user->email ],
            [
                'name'=> $provided_user->name,
                'provider'=>$provider,
                'provider_id'=> $provided_user->id,
            ]
        );

        foreach ($provided_user->user as $key=>$value){
            UserExtra::updateOrCreate(
                [
                    'user_id'=>$user->id,
                    'key'=>$key
                ],
                [
                    'value'=>is_array($value) ? json_encode($value) : $value
                ]
            );
        }

        Auth::loginUsingId($user->id,true);
        return redirect(url(session('redirect')));
    }

    public function username()
    {
        return config('mediapress.auth.username','email');
    }
}
