<?php
namespace Mediapress\Modules\Auth\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('roles')->insert([
                [ 'name' => 'SuperMCAdmin', 'title' => 'Mediaclick Yönetici'],
                [ 'name' => 'Moderator', 'title' => 'Moderatör'],
            ]
        );
    }
}
