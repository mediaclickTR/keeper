<?php

const LOGIN_STR = 'login';
const PREFIX_STR = 'prefix';
Route::get(LOGIN_STR, 'LoginController@showLoginForm')->name("panel.login");

if(class_exists(\Mediapress\VeronLogin\Http\Controllers\Panel\PanelLoginController::class)){
    Route::post(LOGIN_STR, '\Mediapress\VeronLogin\Http\Controllers\Panel\PanelLoginController@login');
}else{
    Route::post(LOGIN_STR, 'LoginController@login');
}

Route::post('logout', 'LoginController@logout')->name('panel.logout');

// Registration Routes...
//Route::get('register', 'PanelRegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'PanelRegisterController@register');

//// Password Reset Routes...
//Route::get('password/reset', 'PanelForgotPasswordController@showLinkRequestForm');
//Route::post('password/email', 'PanelForgotPasswordController@sendResetLinkEmail');
//Route::get('password/reset/{token}', 'PanelResetPasswordController@showResetForm');
//Route::post('password/reset', 'PanelResetPasswordController@reset');


Route::group(['middleware' => 'panel.auth'], function () {
    Route::group([PREFIX_STR => 'Keeper', 'as'=>'Keeper.'], function () {

        Route::group([PREFIX_STR => 'Admins', 'as' => 'admins.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'PanelAdminController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'PanelAdminController@create']);
            Route::post('/store', ['as' => 'store', 'uses' => 'PanelAdminController@store']);
            Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'PanelAdminController@edit']);
            Route::post('/update', ['as' => 'update', 'uses' => 'PanelAdminController@update']);
            Route::get('/ajax/{website_id?}', ['as' => 'ajax', 'uses' => 'PanelAdminController@ajax']);
            Route::get('{id}/delete', ['as' => 'delete', 'uses' => 'PanelAdminController@delete']);
        });

        Route::group([PREFIX_STR => 'Roles', 'as' => 'roles.'], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'PanelRoleController@index']);
            Route::get('/create', ['as' => 'create', 'uses' => 'PanelRoleController@formCreate']);
            Route::get('/{role}/edit', ['as' => 'edit', 'uses' => 'PanelRoleController@edit']);
            Route::post('/{role}/edit', ['as' => 'update', 'uses' => 'PanelRoleController@update']);
            Route::get('/{id}/abilities', ['as' => 'abilities', 'uses' => 'PanelRoleController@abilities2']);
            Route::post('/{id}/storeAbilities', ['as' => 'store_abilities', 'uses' => 'PanelRoleController@storeAbilities']);
            Route::post('/{id}/moduleActions', ['as' => 'module_actions', 'uses' => 'PanelRoleController@ajaxModuleActions']);
            Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'PanelRoleController@delete']);
            Route::get('/ajax', ['as' => 'ajax', 'uses' => 'PanelRoleController@ajax']);
        });

    });
});


