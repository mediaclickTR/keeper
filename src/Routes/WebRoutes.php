<?php

if ( class_exists(config('mediapress.login_controller')) ) {
    Route::post('/login', config('mediapress.login_controller') . '@login')->name('login');
} else {
    Route::get('/login', "LoginController@showLoginForm")->name('loginGet');
    Route::post('/login', "LoginController@login")->name('login');
}

if ( class_exists(config('mediapress.register_controller')) ) {
    Route::post('/register', config('mediapress.register_controller') . '@register')->name('register');
} else {
    Route::get('/register', "RegisterController@showRegistrationForm")->name('registerGet');
    Route::post('/register', "RegisterController@register")->name('register');
}

if ( class_exists(config('mediapress.password_email_controller')) ) {
    Route::post('/password/email', config('mediapress.password_email_controller') . '@sendResetLinkEmail')->name('password_email');
} else {
    Route::post('/password/email', "ForgotPasswordController@sendResetLinkEmail")->name('password_email');
}

if ( class_exists(config('mediapress.password_reset_controller')) ) {
    Route::post('/password/reset', config('mediapress.password_reset_controller') . '@reset')->name('password_update');
} else {
    Route::post('/password/reset', "ResetPasswordController@reset")->name('password_update');
}

if ( class_exists(config('mediapress.email_verify_controller')) ) {
    Route::post('/email/resend', config('mediapress.email_verify_controller') . '@resend')->name('verification_resend');
} else {
    Route::post('/email/resend', "EmailVerifyController@resend")->name('verification_resend');
}

Route::get('email/verify/{id}/{hash}', 'EmailVerifyController@verify')->name('verification_verify');
Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password_reset');

Route::get('/logout', "LoginController@logout")->name('logout');
