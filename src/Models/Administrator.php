<?php

namespace Mediapress\Keeper\Models;

use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class Administrator extends Authenticatable
{
    use HasRolesAndAbilities;

    protected $table = 'admins';
    public $timestamps = true;
    protected $fillable = ["role_id", "language_id", "first_name", "last_name", "username", "email", "phone", "password"];
    protected $hidden = ["password", "remember_token"];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function userable()
    {
        return $this->morphTo();
    }

    public function log()
    {
        return $this->morphMany(UserActionLogs::class, 'model');
    }

}
