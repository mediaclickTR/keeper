<?php

namespace Mediapress\Keeper\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Silber\Bouncer\Database\Role as BRole;
use Illuminate\Support\Facades\Session;

class Role extends BRole
{
    use SoftDeletes;

    public const STATUS = "status";
    protected $fillable = ["name", "title", self::STATUS];

    const PASSIVE = 0;
    const ACTIVE = 1;
    const DRAFT = 2;
    const PREDRAFT = 3;
    const PENDING = 4;

    public static function preCreate($data = [])
    {
        if (Session::has('panel.user')) {

            $model = static::class;
            $model = new $model;

            $admin = Session::get('panel.user');
            $isModel = $model->where("admin_id", $admin->id)->where(self::STATUS, self::PREDRAFT)->first();

            if ($isModel) {

                // Eğer modele ait details varsa temizle
                if ($isModel->details) {
                    $model::deleting(function ($isModel) {
                        $isModel->details()->delete();
                    });
                }

                $isModel->forceDelete();
            }

            $data = array_merge(["admin_id" => $admin->id, self::STATUS => self::PREDRAFT], $data);
            $model->unguarded(function () use ($model, $data, &$isModel) {
                $isModel = $model->create($data);
            });


            return $isModel;
        } else {
            return false;
        }
    }

    public static function scopeStatus($query, $status = self::ACTIVE)
    {
        if (is_array($status)) {
            return $query->whereIn(self::STATUS, $status);
        } else {
            return $query->where(self::STATUS, $status);
        }
    }
}
