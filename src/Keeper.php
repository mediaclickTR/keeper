<?php

namespace Mediapress\Keeper;

use Mediapress\Models\MPModule;

class Keeper extends MPModule
{
    public $name = "Keeper";
    public $url = "mp-admin/Keeper";
    public $description = "Authorization module of Mediapress";
    public $author = "";
    private $plugins = [];

    /**
     * @return mixed
     */
    public function user()
    {
        return session("panel.user");
    }

    public function fillMenu($menu)
    {
        #region Header Menu > Settings > Panel Users > Set

        // Tab Name Set
        data_set($menu, 'header_menu.settings.cols.panel_users.name', trans("KeeperPanel::menu_titles.admins"));

        $set = [
            [
                "type" => "submenu",
                "title" => trans("KeeperPanel::menu_titles.admin_accounts"),
                "url" => route("Keeper.admins.index")
            ],
            [
                "type" => "submenu",
                "title" => trans("KeeperPanel::menu_titles.roles"),
                "url" => route("Keeper.roles.index")
            ]
        ];

        return dataGetAndMerge($menu, 'header_menu.settings.cols.panel_users.rows',$set);
        #endregion


    }
}
