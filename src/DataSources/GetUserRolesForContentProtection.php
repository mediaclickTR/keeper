<?php

namespace Mediapress\Keeper\DataSources;

use Mediapress\Foundation\DataSource;
    use Mediapress\Keeper\Models\Role;
    use Mediapress\Modules\MPCore\Models\Language;

    class GetUserRolesForContentProtection extends DataSource
    {
        public function getData(){
            //TODO:şimdilik panel admin rollerini döndürüyoruz, hazır olduğu zaman user rollerine çevrilecek
            $pluck=($this->params["pluck"] ?? false) && $this->params["pluck"];
            if($pluck){
                $key = $this->params["pluck_key"] ?? "id";
                $value = $this->params["pluck_column"] ?? "id";
                return Role::status()->orderby("title")->get()->pluck($value,$key);
            }
            return Role::status()->orderby("title")->get();
        }
    }
